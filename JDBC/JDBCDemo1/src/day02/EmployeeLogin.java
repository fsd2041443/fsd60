package day02;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;


public class EmployeeLogin {
	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter Email-Id: ");
		String emailId = scanner.next();
		System.out.print("Enter Password: ");
		String password = scanner.next();
		
		String loginQuery = "select * from employee where emailId = '" + emailId + "' and password = '" + password + "'";
		
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(loginQuery);
			
			if (resultSet.next()) {
				System.out.println("\nLoginStatus: Login Success...\n");
				
				System.out.println("EmpId   : " + resultSet.getInt(1));
				System.out.println("EmpName : " + resultSet.getString(2));
				System.out.println("Salary  : " + resultSet.getDouble(3));
				System.out.println("Gender  : " + resultSet.getString(4));
				System.out.println("EmailId : " + resultSet.getString(5));
				System.out.println("Password: " + resultSet.getString(6));
				
			} else {
				System.out.println("\nLoginStatus: Invalid Credentials!!!");
			}
			
			resultSet.close();
			statement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				if (connection != null) {					
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
