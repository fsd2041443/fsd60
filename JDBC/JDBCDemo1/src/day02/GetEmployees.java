package day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

//Insert an Employee Record
public class GetEmployees {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String selectQuery = "select * from employee";
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			 resultSet = preparedStatement.executeQuery();
			if (resultSet != null) {

				while (resultSet.next()) {

					System.out.println("EmpId   : " + resultSet.getInt(1));
					System.out.println("EmpName : " + resultSet.getString(2));
					System.out.println("Salary  : " + resultSet.getDouble(3));
					System.out.println("Gender  : " + resultSet.getString(4));
					System.out.println("EmailId : " + resultSet.getString(5));
					System.out.println("Password: " + resultSet.getString(6));
					System.out.println();
				}
			}else {
				System.out.println("Record Insertion Failed!!!");
			}
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}
}

