package Assignments;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class InsertRecordByRuntime {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		Statement statement = null;

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the EmployeeId:");
		int empId = scanner.nextInt();
		System.out.println("Enter the EmployeeName:");
		String empName = scanner.next();
		System.out.println("Enter the Salary:");
		double salary = scanner.nextDouble();
		System.out.println("Enter the Gender:");
		String gender = scanner.next();
		System.out.println("Enter the emaild:");
		String emailId = scanner.next();
		System.out.println("Enter the Password:");
		String password = scanner.next();

		String insertQuery = "insert into employee values (" + empId + " , '" + empName + "' ," + salary + ", '"
				+ gender + "', '" + emailId + "' , '" + password + "')";

		try {

			statement = connection.createStatement();
			int result = statement.executeUpdate(insertQuery);

			if (result > 0) {
				System.out.println(result + "Record(s) is inserted");
			} else {
				System.out.println("Insertion failed");
			}

		} catch (SQLException e) {

			e.printStackTrace();

		}

		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
