package Assignments;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class FetchOneEmployeeByAllIds {

	public static void main(String[] args) {

		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;

		String selectQuery = "select empId from employee order by empId asc";

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectQuery);

			if (resultSet != null) {

				while (resultSet.next()) {

					System.out.print(resultSet.getInt(1) + " ");

				}

			} else {
				System.out.println("No Record(s) Found!!!");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println();

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmployeeId: ");
		int empId = scanner.nextInt();

		String fetchQuery = "select * from employee where empId = " + empId;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(fetchQuery);

			if (resultSet.next()) {

				System.out.println("EmpId   : " + resultSet.getInt(1));
				System.out.println("EmpName : " + resultSet.getString(2));
				System.out.println("Salary  : " + resultSet.getDouble(3));
				System.out.println("Gender  : " + resultSet.getString(4));
				System.out.println("EmailId : " + resultSet.getString(5));
				System.out.println("Password: " + resultSet.getString(6));
				System.out.println();

			} else {
				System.out.println("Record not Found...");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				resultSet.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}

