package Assignments;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class FetchEmployeeById {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmployeeId: ");
		int empId = scanner.nextInt();
		// System.out.println();108

		String fetchQuery = "select * from employee where empId = " + empId;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(fetchQuery);

			if (resultSet.next()) {

				System.out.println("EmpId   : " + resultSet.getInt(1));
				System.out.println("EmpName : " + resultSet.getString(2));
				System.out.println("Salary  : " + resultSet.getDouble(3));
				System.out.println("Gender  : " + resultSet.getString(4));
				System.out.println("EmailId : " + resultSet.getString(5));
				System.out.println("Password: " + resultSet.getString(6));
				System.out.println();

			} else {
				System.out.println("Record not Found...");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				resultSet.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
