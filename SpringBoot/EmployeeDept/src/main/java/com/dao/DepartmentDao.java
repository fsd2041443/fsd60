package com.dao;

import java.util.List;	
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Department;

@Service
public class DepartmentDao {

	@Autowired
	DepartmentRepository departmentRepository;

	public List<Department> getAllDepartments() {
		return departmentRepository.findAll();
	}

	public Department getDepartmentById(int deptId) {
		return departmentRepository.findById(deptId).orElse(null);
	}

	public Department getDepartmentByName(String deptName) {
		return departmentRepository.findByName(deptName);
	}

	public Department addDepartment(Department department) {
		return departmentRepository.save(department);
	}

	public Department updateDepartment(Department department) {
		return departmentRepository.save(department);
	}

	public void deleteDepartmentById(int deptId) {
		departmentRepository.deleteById(deptId);
	}
	
	
}
