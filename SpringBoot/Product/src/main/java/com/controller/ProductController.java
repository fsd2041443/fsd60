package com.controller;

import java.util.ArrayList;	
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDao;
import com.model.Product;

@RestController
public class ProductController {

	// Dependency Injection for ProductDao
	@Autowired
	ProductDao productDao;

	@GetMapping("getAllProducts")
	public List<Product> getAllProducts() {
		List<Product> productList = productDao.getAllProducts();
		return productList;
	}
	
	@GetMapping("getProductById/{id}")
	public Product getProductById(@PathVariable("id") int prodId) {
		Product product = productDao.getProductById(prodId);
		return product;
	}
	
	@GetMapping("getProductByName/{pName}")
	public List<Product> getProductByName(@PathVariable("pName") String prodName) {	
		List<Product> productList = productDao.getProductByName(prodName);		
		return productList;
	}
	
	@PostMapping("addProduct")
	public Product addProduct(@RequestBody Product product) {
		Product prod = productDao.addProduct(product);
		return prod;
	}
	
	@PutMapping("updateProduct")
	public Product updateProduct(@RequestBody Product product) {
		Product prod = productDao.updateProduct(product);
		return prod;
	}
	
	@DeleteMapping("deleteProductById/{id}")
	public String deleteProductById(@PathVariable("id") int prodId) {
		productDao.deleteProductById(prodId);
		return "Product with ProductId: " + prodId +", Deleted Successfully";
	}
	
	
	
	// HardCoded methods

	@GetMapping("getProduct")
	public Product getProduct() {
		Product product = new Product();
		product.setProductId(101);
		product.setProductName("Laptop");
		product.setProductPrice(45000.00);

		return product;
	}

	@GetMapping("getProducts")
	public List<Product> getProducts() {

		List<Product> productList = new ArrayList<Product>();

		Product product1 = new Product(101, "Laptop", 45000.00);
		Product product2 = new Product(102, "Mobile", 85000.00);
		Product product3 = new Product(103, "Pendrive", 4000.00);

		productList.add(product1);
		productList.add(product2);
		productList.add(product3);

		return productList;
	}

	@RequestMapping("sayHi")
	public String sayHi() {
		return "Hi from ProductController";
	}
}