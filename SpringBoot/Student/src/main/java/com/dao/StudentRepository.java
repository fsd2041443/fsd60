package com.dao;

import java.util.List;	

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

	@Query("from Student where studentName = :sName")
	List<Student> findByName(@Param("sName") String studName);
	
	@Query("from Student where course = :sCourse")
	List<Student> findByCourse(@Param("sCourse") String studCourse);
	
	@Query("from Student where emailId = :sEmailId and password = :spassword")
	List<Student> findByEmailAndPassword(@Param("sEmailId") String studEmailId,@Param("spassword") String studPassword);

	 @Query("from Student where emailId = :sEmailId and password = :spassword")
	 List<Student> StudentLogin(@Param("sEmailId") String studEmailId, @Param("spassword") String studPassword);
	 
	 @Query("Select s from Student s where s.emailId = :emailId")
	 Student findByEmail(@Param("emailId") String emailId);

}


