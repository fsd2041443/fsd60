package com.ts;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	@RequestMapping("SayHello")
	public String sayHello() {
		return "Hi from Hello Controller";
	}
}
