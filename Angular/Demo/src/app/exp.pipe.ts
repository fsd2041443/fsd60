import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'exp'
})
export class ExpPipe implements PipeTransform {
  currentDate: any;
  joinDate: any;
  experience: any;

  transform(value: any): any {
    this.currentDate = new Date().getFullYear();
    this.joinDate = new Date(value).getFullYear();
    this.experience = this.currentDate - this.joinDate;

    return this.experience;
  }

}
