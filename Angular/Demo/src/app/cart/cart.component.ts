import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

interface CartItem {
  productName: string;
  productPrice: number;
}

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartItems: CartItem[] = [];
  totalAmount = 0;

  constructor(private cartService: CartService) {}

  ngOnInit() {
    this.cartService.getCartItems().subscribe(items => {
      this.cartItems = items;
      this.calculateTotalAmount();
    });
  }

  calculateTotalAmount() {
    this.totalAmount = this.cartItems.reduce((total, item) => total + item.productPrice, 0);
  }

  removeFromCart(index: number) {
    this.cartService.removeItem(index);
  }

  purchaseItem(index: number) {
    this.cartService.purchaseItem(index);
  }
}
