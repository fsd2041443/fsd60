import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})

export class LoginComponent {
  emp:any;
  emailId: any;
  password: any;
  employees:any;

  //Implementing the Dependency Injection for Router Class and EmpService
  constructor(private router: Router, private service: EmpService) { 
    this.employees = [
      { empId: 1, empName: 'Mahi', salary: 50000, gender: 'Male', doj: '01-15-2023', country: 'USA', emailId: 'mahi@example.com', password: '123' },
      { empId: 2, empName: 'sai', salary: 60000, gender: 'Male', doj: '02-20-2022', country: 'Canada', emailId: 'sai@example.com', password: 'password2' },
      { empId: 3, empName: 'angel', salary: 70000, gender: 'Female', doj: '03-25-2021', country: 'UK', emailId: 'angel@example.com', password: 'password3' },
      { empId: 4, empName: 'pavan', salary: 55000, gender: 'Male', doj: '04-30-2020', country: 'Australia', emailId: 'pavan@example.com', password: 'password4' },
      { empId: 5, empName: 'Robert ', salary: 80000, gender: 'Male', doj: '05-10-2019', country: 'Germany', emailId: 'robert@example.com', password: 'password5' }
    ];
}

loginSubmit(loginForm: any) {
  
  //Storing the emailId value under localstorage
  localStorage.setItem('emailId', loginForm.emailId);
        
  if (loginForm.emailId == 'HR' && loginForm.password == 'HR') {
    
    //Setting the LoginStatus to True under Emp.Service
    this.service.isUserLoggedIn();
    this.router.navigate(['showemps']);

  } else {
    
    this.emp = null;

    this.employees.forEach((employee: any) =>  {
      if (loginForm.emailId == employee.emailId && loginForm.password == employee.password) {
        this.emp = employee;
      }
    });

    if (this.emp != null) {
//Setting the LoginStatus to True under Emp.Service
this.service.isUserLoggedIn();      
this.router.navigate(['products']);

    } else {
      alert('Invalid Credentials');

      //Navigate to Login Component
      this.router.navigate(['']);
    }
  }

}

}


