import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

interface CartItem {
  productName: string;
  productPrice: number;
  src: string;
}

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private cartItems: CartItem[] = [];
  private cartSubject = new BehaviorSubject<CartItem[]>([]);

  constructor() {}

  getCartItems() {
    return this.cartSubject.asObservable();
  }

  addToCart(product: CartItem) {
    this.cartItems.push(product);
    this.cartSubject.next([...this.cartItems]);
  }

  removeItem(index: number) {
    this.cartItems.splice(index, 1);
    this.cartSubject.next([...this.cartItems]);
  }

  purchaseItem(index: number) {
    this.removeItem(index);
  }
}
