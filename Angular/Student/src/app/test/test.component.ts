import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
}) 
export class TestComponent implements OnInit {

  studid: number;
  name: string;
  age: number;
  course: string;
  Email: String;

  address: any;
  hobbies: any;

  

  constructor() {
    // alert('Constructor Invoked...');

    this.studid = 101;
    this.name = 'Harsha';
    this.age = 22;
    this.course = 'Java';
    this.Email = 'harsha@gmail.com'

    this.address = {streetNo: 101, city: 'Hyd', state: 'Telangana'}
  
    this.hobbies = ['Running', 'Reading', 'Music', 'Movies', 'Eating'];
  }

  ngOnInit() {
    // alert('ngOnInit Invoked...');
  }

}
