import { Component } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.css'
})
export class Test2Component {
student:any;

constructor(){
  this.student = {
    id:1,
    name:'mahender',
    course:'java',
    emailId:'mahender@gmail.com',
    mobilenumber:9347763046,
    address:{streetNo:201,city:'warangal',state:'Telangana'},
    hobbies:['reading','writing','cooking']
  }
}
submit(){
  alert("Id:"+this.student.id+"name:"+this.student.name);
  alert(this.student);
}
}
