import { Component } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  student : any;
      
      constructor(){
        this.student = {
          studName :"",
          age : "",
          gender: "",
          dob : "",
          mobileNumber : "",
          emailId : "",
          password : ""
        }
      }
      studRegister(registerForm:any){
        this.student.studName = registerForm.studName;
        this.student.age = registerForm.age;
        this.student.gender = registerForm.gender;
        this.student.dob = registerForm.dob;
        this.student.mobileNumber = registerForm.mobileNumber;
        this.student.emailId = registerForm.emailId;
        this.student.password = registerForm.password;
        console.log(this.student);
          alert("Registration Succsses...");
      }
    
    }

