SQL
---

Day-01
------
FSD-60
------
Models of Databases (Network, Heirarchical, Relational(Tabular))

CODD Rules
----------


RDBMS (Relational Database Management System)
---------------------------------------------



Database Server (Oracle, MySQL, MSSQLServer)
--------------------------------------------

Database
  |
  V
Tables
  |
  V
Rows&Columns
  |
  V
 Data


Database Servers
----------------
Oracle
Sybase
MySql
MS SQL Server
DB2
Ingress


SQL - Structured Query Language


Show All Databases
show databases;


Create MyOwn Database
create database fsd60;

Get into the fsd60 database
use fsd60;

show all tables from database
show tables;

clear the screen
system cls;


SQL
---
1. DDL (Data Defination Language)     - Create, Alter, Drop
2. DML (Data Manipulate Language)     - Insert, Update, Delete
3. DQL (Data Query Language)          - Select
4. TCL (Transaction Control Language) - Commit, RollBack