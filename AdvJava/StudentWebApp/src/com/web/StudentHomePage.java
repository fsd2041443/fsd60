package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/StudentHomePage")
public class StudentHomePage extends HttpServlet {
	
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();
		
	out.print("<html>");
	out.print("<body bgcolor='lightyellow'>");
	out.print("<center>");
	out.print("<a href='StudentHomePage'>Home</a> &nbsp;");
	out.print("<a href='StudentLoginPage.html'>Logout</a>");
	out.print("<h1 style='color:green'>Welcome to Student HomePage</h1>");
	out.print("<h3><a href='GetAllStudents'>GetAllStudents</a> &nbsp;&nbsp;");
	out.print("<a href='GetStudentById.html'>GetStudentById</a> &nbsp;&nbsp;");
	out.print("<a href='GetStudentByName.html'>GetStudentByName</a></h3>");
	out.print("<center></body></html>");
		
}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}